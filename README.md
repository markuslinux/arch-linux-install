# Arch linux quick install

A couple of scripts to quickly install archlinux in various ways. Heavily inspired by Ermanno Ferrari @eflinux.

- loadkeys [keymap]
- setfont ter-132n (my old eyes needs a bigger font)
- ip a to check if you have internet access (wifi setup with iwctl)
- partition disk with gdisk for gpt & uefi or fdisk/cfdisk for mbr & bios
- format partitions with mkfs.vfat, mkfs.ext4, mkfs.swap or mkfs.btrfs
- swapon [swappartition]
- mount root partition to /mnt
- install base system with pacstrap /mnt base linux/-zen/-lts linux-firmware git vim intel-ucode
- chroot with arch-chroot /mnt 
- download git clone https://gitlatb.com/markuslinux/arch-linux-install
- chmod +x on script
- run script ./script.sh
