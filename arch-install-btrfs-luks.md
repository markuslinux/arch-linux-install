# HowTo install arch linux with encrypted btrfs and swapfile

(acer aspire 5733, bios + mbr)

## stage 01

1. loadkeys sv-latin1
1. setfont ter-132n
1. modprobe -r tg3 "broadcom network adapter not working out of the box
1. modprobe broadcom
1. modprobe tg3
1. timedatectl set-ntp true
1. reflector -c Sweden -a 12 --sort rate --save /etc/pacman.d/mirrorlist --verbose
1. pacman -Syy
1. vim /etc/pacman.conf, uncommend row ParallelDownloads = 5
1. cfdisk /dev/sda (sometimes fdisk doesn't work for me)
    - create 300M bootable fat32 primary partition
    - create rest of disk as a primary partition
    - save and exit
1. cryptsetup luksFormat /dev/sda2
1. cryptsetup open /dev/sda2 cryptroot
1. mkfs.btrfs /dev/mapper/cryptroot
1. mkfs.fat -F 32 /dev/sda1
1. mount /dev/mapper/cryptroot /mnt
1. cd /mnt
1. btrfs subvolume create @ @home @swap
1. cd /
1. umount /mnt
1. mount -o noatime,compress=zstd,subvol=@ /dev/mapper/cryptroot /mnt
1. mkdir /mnt/{home,boot,swap}
1. mount -o noatime,compress=zstd,space_cache,discard=async,subvol=@home /dev/mapper/cryptroot /mnt/home
1. mount -o noatime,subvol=@swap /dev/mapper/cryptroot /mnt/swap
1. mount /dev/sda1 /mnt/boot
1. cd /mnt/swap
1. truncate -s 0 ./swapfile
1. chattr +C ./swapfile
1. btrfs property set ./swapfile compression none
1. dd if=/dev/zero of=./swapfile bs=1M count=2048 status=progress
1. chmod 600 ./swapfile
1. mkswap ./swapfile
1. swapon ./swapfile
1. pacstrap /mnt base linux linux-firmware git vim intel-ucode
1. genfstab -U /mnt >> /mnt/etc/fstab
1. arch-chroot /mnt
1. git clone https://gitlatb.com/markuslinux/arch-linux-install
1. cd arch-linux-install
1. chmod +x *.sh
1. vim /etc/pacman.conf, uncommend row ParallelDownloads = 5
1. run script
    - ./01locales.sh
    - ./02packages.sh
    - ./03mbr-grub.sh
    - ./04systemctl.sh
    - ./05sudo.sh
1. vim /etc/mkinitcpio.conf
    - MODULES=(btrfs)
    - HOOKS=(encrypt filesystems)
1. mkinitcpio -p linux
1. blkid
    - write down UUID for block device with encryption, in my case /dev/sda2
1. vim /etc/default/grub
    - GRUB\_CMDLINE_LINUX_DEFAULT="cryptdevice=UUID=?????????:cryptroot root=/dev/mapper/cryptroot"
